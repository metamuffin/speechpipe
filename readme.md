# speechpipe

Ultra low-bandwidth voice chat (if you can even call it that). Implement by
coupling STT with TTS over TCP.

Uses OpenAI's Whisper model to convert your speech to text, send it over TCP and
let it be played back on the other side with espeak or festival.

Whisper is implemented with rust bindings to ggerganov's ggml.

## Usage

1. Install this project with
   `cargo install --git https://codeberg.org/metamuffin/speechpipe`. Enable one
   of the features `hipblas`, `opencl` or `openblas` if you system supports it.
2. Download a the whisper model (instructions found in the
   [whisper.cpp repo](https://github.com/ggerganov/whisper.cpp))
3. Host a TCP server that broadcasts to all clients. (`ncat --broker <port>`)
4. Run `speechpipe <host>:<port>`

```
Usage: speechpipe [OPTIONS] <SERVER>

Arguments:
  <SERVER>  Address of the chat server

Options:
  -m, --model <MODEL>
          Path to whisper model in ggml format [default: models/ggml-base.bin]
      --threads <THREADS>
          [default: 6]
      --speech-above <SPEECH_ABOVE>
          Speech detection upper threshold for activation [default: 10]
      --silence-below <SILENCE_BELOW>
          Speech detection lower threshold for deactivation [default: 1]
      --vad-samples <VAD_SAMPLES>
          Samples taken for volume measurement [default: 1600]
      --deativate-delay <DEATIVATE_DELAY>
          Keep voice activation longer than you talk to avoid cutoff [default: 3]
  -h, --help
          Print help
```
