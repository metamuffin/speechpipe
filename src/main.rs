/*
    This file is part of speechpipe (https://codeberg.org/metamuffin/speechpipe)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin@disroot.org>
*/
use clap::Parser;
use cpal::{
    traits::{DeviceTrait, HostTrait, StreamTrait},
    StreamConfig,
};
use log::{debug, info, warn};
use nnnoiseless::DenoiseState;
use std::{
    collections::VecDeque,
    io::{BufRead, BufReader, Write},
    net::{SocketAddr, TcpStream},
    sync::mpsc,
};
use whisper_rs::{FullParams, SamplingStrategy, WhisperContext, WhisperContextParameters};

#[derive(Parser)]
struct Args {
    /// Address of the chat server
    server: SocketAddr,
    /// Path to whisper model in ggml format
    #[arg(short, long, default_value = "models/ggml-base.bin")]
    model: String,

    #[arg(long, default_value_t = 6)]
    threads: i32,

    /// Speech detection upper threshold for activation
    #[arg(long, default_value_t = 10.0)]
    speech_above: f32,
    /// Speech detection lower threshold for deactivation
    #[arg(long, default_value_t = 1.0)]
    silence_below: f32,
    /// Samples taken for volume measurement
    #[arg(long, default_value_t = 16000 / 10)]
    vad_samples: usize,
    /// Keep voice activation longer than you talk to avoid cutoff.
    #[arg(long, default_value_t = 3)]
    deativate_delay: usize,
}

fn main() {
    env_logger::Builder::new()
        .filter_level(log::LevelFilter::Info)
        .parse_env("LOG")
        .init();
    let args = Args::parse();
    let device = cpal::default_host().default_input_device().unwrap();

    let (raw_tx, raw_rx) = mpsc::channel();
    let (speech_tx, speech_rx) = mpsc::channel();

    let sock = TcpStream::connect(&args.server).unwrap();

    let stream = device
        .build_input_stream(
            &StreamConfig {
                channels: 1,
                sample_rate: cpal::SampleRate(16_000),
                buffer_size: cpal::BufferSize::Default,
            },
            move |data: &[f32], _info| raw_tx.send(data.to_vec()).unwrap(),
            |err| eprintln!("{err:?}"),
            None,
        )
        .unwrap();
    stream.play().unwrap();

    std::thread::spawn(move || {
        let mut inbuf = VecDeque::<f32>::new();
        let mut tempbuf = [0.; DenoiseState::FRAME_SIZE];
        let mut outbuf = VecDeque::<f32>::new();
        let mut speechbuf = Vec::<f32>::new();
        let mut denoiser = DenoiseState::new();
        let mut active = false;
        let mut deactivate_timer = 0;

        for data in raw_rx {
            inbuf.extend(data);
            while inbuf.len() > DenoiseState::FRAME_SIZE {
                let frame = inbuf.drain(0..DenoiseState::FRAME_SIZE).collect::<Vec<_>>();
                denoiser.process_frame(&mut tempbuf, &frame);
                outbuf.extend(tempbuf)
            }
            while outbuf.len() > args.vad_samples {
                let mut sd = 0.;
                let mut last = 0.;
                for s in outbuf.range(0..args.vad_samples) {
                    sd += (*s - last).abs();
                    last = *s;
                }
                let frame = outbuf.drain(0..args.vad_samples);
                let last_active = active;
                active = match active {
                    false => sd > args.speech_above,
                    true => sd > args.silence_below,
                };
                if active {
                    deactivate_timer = args.deativate_delay
                } else if deactivate_timer > 0 {
                    deactivate_timer -= 1;
                    debug!("dtimer {deactivate_timer}");
                    active = true
                }
                debug!("{active} {}", sd);
                if active {
                    speechbuf.extend(frame);
                }
                if !last_active && active {
                    info!("speech start");
                }
                if last_active && !active {
                    info!("speech end");
                    let mut s = Vec::new();
                    std::mem::swap(&mut s, &mut speechbuf);
                    speech_tx.send(s).unwrap();
                }
            }
        }
    });

    let mut sock2 = sock.try_clone().unwrap();
    std::thread::spawn(move || {
        let ctx = WhisperContext::new_with_params(&args.model, WhisperContextParameters::default())
            .expect("failed to load model");
        let mut state = ctx.create_state().unwrap();
        for audio in speech_rx {
            debug!("whisper run");
            let mut params = FullParams::new(SamplingStrategy::Greedy { best_of: 1 });
            params.set_print_progress(false);
            params.set_single_segment(true);
            params.set_n_threads(args.threads);
            state.full(params, &audio[..]).expect("failed to run model");
            let num_segments = state.full_n_segments().unwrap();

            for i in 0..num_segments {
                let segment = state
                    .full_get_segment_text(i)
                    .expect("failed to get segment");
                info!("-> {segment}");
                sock2.write_all(format!("{segment}\n").as_bytes()).unwrap();
            }
            if num_segments == 0 {
                warn!("no segments")
            }
        }
    });

    for line in BufReader::new(sock).lines() {
        let line = line.unwrap();
        info!("<- {line}");
        println!("{line}");
    }
}
